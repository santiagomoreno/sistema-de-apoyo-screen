    var bootstrap = function() {

    var map;
    var cluster;
    var listaCentros;
    var range = [-34.5221554, -58.7000067];

    // Ubicación de la UNGS.
    
    listaCentros=[];  
    iniciarDatos();
  
    // Creación del componente mapa de Leaflet.
    map = L.map('mapid').setView(range, 12);

    // Agregamos los Layers de OpenStreetMap.
    var baseLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);


    var layersControl = L.control.layers({
        "Base": baseLayer
    });
    layersControl.addTo(map);

    map.layersControl = layersControl;

    var ungsPopUp = "Universidad Nacional de General Sarmiento";
    // creamos un poligono sobre la UNGS
    L.polygon([
        L.latLng(-34.52049, -58.69985),
        L.latLng(-34.52189, -58.69819),
        L.latLng(-34.52372, -58.70007),
        L.latLng(-34.52246, -58.70184)
    ]).addTo(map);

    var ungsLocation = [-34.5221554, -58.7000067];
    var ungsMarker = L.marker(ungsLocation).bindPopup(ungsPopUp);
    ungsMarker.addTo(map);
    
    cluster=L.markerClusterGroup();
      
    mostrarCentros();


    function iniciarDatos(){
      //Creacion y listado de Comercios
        var centro1=new Centro("Centro de asistencia 1","Darragueira 962","555-9292",[-34.518312, -58.708354]);
        var centro2=new Centro("Centro de asistencia 2","Pablo Rojas Paz 1901","555-6262",[-34.518335, -58.701153]);
        var centro3=new Centro("Centro de asistencia 3","Pablo Rojas Paz 2301","555-3131",[-34.516099, -58.701902]);
        var centro4=new Centro("Centro de asistencia 4","Carlos Pellegrini 900","555-1111",[-34.522050, -58.703528]);
        var centro5=new Centro("Centro de asistencia 5","Verdi 1700","555-6565",[-34.523730, -58.699539]);
        var centro6=new Centro("Centro de asistencia 6","Los Robles 1701","555-3231",[-34.518081, -58.696131]);
        var centro7=new Centro("Centro de asistencia 7","John F. Kennedy 800-602","555-6966",[-34.527760, -58.698141]);
    

        listaCentros.push(centro1); 
        listaCentros.push(centro2);
        listaCentros.push(centro3);
        listaCentros.push(centro4);
        listaCentros.push(centro5); 
        listaCentros.push(centro6);
        listaCentros.push(centro7);
    }
    //se dibuja con el drawer los centros hardcordeados
    function mostrarCentros(){
        var listCoord=[];
        var d=new Drawer();

        for(var centro of listaCentros){
            listCoord.push(d.drawCentroInfo(centro,map));
        }
   
        cluster.addLayers(listCoord);
        cluster.addTo(map);
    
    }
      
};

$(bootstrap);

  
  
