var Drawer = function() {
    return {  
        drawCentroInfo:drawCentroInfo
    }
	//Se dibuja un marker del centro en el mapa
    function drawCentroInfo(Centro,map) {
        var Coordenadas=Centro.coord;
       
		// Creamos un marker.		
		var p = L.marker(Coordenadas).on('click', function(e){
	    //Cuando se hace click en el marker muestra esta informacion en pantalla
                    document.getElementById("nombre").innerHTML = Centro.nombre;
                    document.getElementById("direccion").innerHTML = "Dirección: "+Centro.direccion;
                    document.getElementById("telefono").innerHTML = "Telefono: "+Centro.telefono;
                });
                
        return p;
    }
}
