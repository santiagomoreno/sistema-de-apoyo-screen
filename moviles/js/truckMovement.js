var truckMovement = function(name, map) {
    this.name = name;
    this.map = map;
    this.supportTrucksData = [];
    

    this.addTrucks=function (SupportTruck,states) {
        //Creamos el layer en el mapa para ese runner
        var movilesLayer = L.layerGroup().addTo(this.map);
        // Agregamos el layer al control
        this.map.layersControl.addOverlay(movilesLayer, SupportTruck.id);
         
         //aca vamos a ir actualizando las posiciones de los moviles
        var updater = function(newPosition) {
           
            //borramos la posicion anterior
            movilesLayer.clearLayers();	
            //seteamos el valor que tiene el popup de cada movil
            var newpopup = L.popup({
                closeOnClick: false,
                autoClose: false
            }).setContent("Movil nº "+SupportTruck.id);
            //creamos el marcador
            var Marker =new L.Marker((newPosition.position), {
                icon: new L.NumberedDivIcon({number : SupportTruck.id })
            });


            //le ponemos el marcador a la posicion del movil
            movilesLayer.addLayer(Marker);
            //imprimimos al lado del mapa la informacion del estado
            document.getElementById(SupportTruck.id).innerHTML = "<br>Movil Nº "+SupportTruck.id+"<br>Estado: "+states[newPosition.state].description;
           
        }
        //pusheamos al arreglo (no entiendo esta parte)
        this.supportTrucksData.push({
            SupportTruck: SupportTruck,
            updater: updater
        })
    }
	
  //aca ejecutamos e imprimimos todo el movimiento de los moviles   
  this.start = function() {
    this.supportTrucksData.forEach(function(data) {
        var SupportTruck = data.SupportTruck;
        SupportTruck.run(data.updater);
    });
  }
}
