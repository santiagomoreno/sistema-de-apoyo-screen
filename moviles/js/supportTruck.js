//CLASE
//guarda un id y un historial de posiciones con state y position(con latitud y longitud)
var SupportTruck = function(id, historyPositions) {
    this.id =id;
    this.historyPositions = historyPositions;
    
    var actualIx = 0;//este es la posicion actual

    //va recorriendo las posiciones del historyPositions
    this.run = function(callback) {
        var self = this;
        setTimeout(function() {
            callback(historyPositions[actualIx]);

            actualIx += 1;
            if(actualIx < historyPositions.length) {
                self.run(callback);
            }
        }, 1000);
    }
};

