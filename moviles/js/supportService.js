
/******************************************************************************
 * Funciones para request asincrónico utilizando XMLHttpRequest
 */
var asyncQuery = function(url, callback) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        // https://stackoverflow.com/questions/13293617/why-is-my-ajax-function-calling-the-callback-multiple-times
        if (this.readyState === 4) {
            if (this.status === 200) {
                // parseamos el resultado para obtener el objeto JavaScript
                resObj = JSON.parse(xhttp.responseText)
                // llamamos a la función callback con el objeto parseado como parámetro.
                callback(resObj);
            }
        }
    };
    xhttp.open("GET", url, true);
    var ret = xhttp.send();
    return ret;
}


/******************************************************************************
 * Inicio.
 */
var bootstrap = function() {
    var url = Config.url;
	var urlTrucks = '/supporttrucks/';
	var urlStates = '/truckstates/';
    var urlPositions = '/positions/';
    
    var ungsLocation = [-34.5221554, -58.7000067];

    // Creación del componente mapa de Leaflet.
    var map = L.map('mapid').setView(ungsLocation, 14);

    // Agregamos los Layers de OpenStreetMap.
    var baseLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    // Agregamos el control para seleccionar Layers al mapa
    var layersControl = L.control.layers({
        "Base": baseLayer
    });
    layersControl.addTo(map);
    // hack:
    map.layersControl = layersControl;

    
    var drawer = new Drawer();

    //cacheamos los id de moviles
    var idTrucks = [];
    var trucksArray=[];
    var states=[];
        
    //
    var ungsPopUp = "Universidad Nacional de General Sarmiento";
    // creamos un poligono sobre la UNGS
    L.polygon([
        L.latLng(-34.52049, -58.69985),
        L.latLng(-34.52189, -58.69819),
        L.latLng(-34.52372, -58.70007),
        L.latLng(-34.52246, -58.70184)
    ]).addTo(map);

    
    //ubicacion de la ungs
    var ungsLocation = [-34.5221554, -58.7000067];
    
    var ungsMarker = L.marker(ungsLocation).bindPopup(ungsPopUp);
    ungsMarker.addTo(map);
    
	//Se crea un recorrido
    var recorrido = new truckMovement("Recorrido", map);

  //----Realiza request asincronico de los estados------------------------------------------------------------------------------------
    var callback = function(response) {
		
		//Recibe como respuesta los estados de los moviles y los guarda en un arrray(disponible, no operativo, en servicio)
        var states = response.states.reduce(function(dict, state) {
            dict[state.id] = state;			
            states=dict;            
			return dict;
		}, {});

			
        
        //Se pide los datos de los moviles
        requestAllSupportTruckId().then(function(){
            for(var id of idTrucks){
                asyncQuery(url + urlTrucks + id , callback2);
            }
        }).then(function(){getTrucks()}).then(setTimeout(function(){
         //Se recorren el array que contiene los moviles al recorrido   
                for(var truck of trucksArray){
                    recorrido.addTrucks(truck,states);
                    g = document.createElement('div');
                    g.setAttribute("id", truck.id);
                    document.getElementById("states").appendChild(g); 
                }
				//se inicia el recorrido
                recorrido.start()
     
        }, 5000));//lleno el TrucksArray con los id y positions de los moviles
     

    };
//----Realiza request asincronico de los moviles------------------------------------------------------------------------------------------------------
	var callback2 = function(response) {
			var supportTruck = response.supportTruck;
            supportTruck.state = states[response.supportTruck.state_id];            
            delete supportTruck.state_id;
        }
	
	//inicio del pedido
    asyncQuery(url + urlStates, callback);
//------------------------------------------------------------------------------------------------------
//traigo todos los id de los moviles
//------------------------------------------------------------------------------------------------------
 	
   
    var requestAllSupportTruckId = function() {
        var json;
        return $.getJSON(url+urlTrucks, function(result){
            json = result.supportTrucks;
            $.each(json, function(index,element){
                idTrucks.push(element.id)
            });
        });
    }
    //hago la request al sistema externo, todas las posiciones y stados del id que reciba 
    var requestPositions = function(supportTruck_id) {
        return $.ajax(url + urlTrucks + supportTruck_id+ urlPositions);
    }
//------------------------------------------------------------------------------------------------------
// traigo todos las posiciones de todos los id conseguidos y guardo todo en el array SupportTrucks 
//------------------------------------------------------------------------------------------------------
 
    var getTrucks=function(){
        for(var value of idTrucks){
            requestPositions(value).then(function(result){
                    var aux=new SupportTruck(result.truck_id,result.positions);
                    trucksArray.push(aux);
            });
         }
     }
     
   
 
};

$(bootstrap);
