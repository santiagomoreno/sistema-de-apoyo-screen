function inicio() {
    var urllocal = Config.urllocal; // guardo la url de la pagnia de alertas locales en una variable, la saco del archivo Config.js
    var urlClima = Config.urlclima; // guardo la url de la pagina del clima en una variable, la saco del archivo Config.js
    var urlNac = Config.urlnac; // guardo la url de la pagina de alertas nacionales en una variable, la saco del archivo Config.js
    var urlalertas = '/alerts/day/'; // texto que se le agrega a la url de alertas locales para buscar las alertas

    // ahora declaro 3 request xmlhttp una para las alertas locales, otra para las nacionales y una para el clima    
    var xmlhttp1 = new XMLHttpRequest();
    var xmlhttp2 = new XMLHttpRequest();
    var xmlhttp3 = new XMLHttpRequest();

    var url = urllocal + urlalertas + '3'; // completo la url de alertas locales y le agrego el dia del que pido las alertas

    // esto se ejecuta cuando se reliza el llamado xmlhttp de las alertas locales
    xmlhttp1.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) { // si el llamado se realizo bien entonces:
            var myArr = JSON.parse(this.responseText); // se parsea la respuesta a myArr
            cargarAlertaslocales(myArr); // se llama a la funcion que carga las alertas locales en la pagina
        }
    };
    // esto se ejecuta cuando se reliza el llamado xmlhttp de las alertas nacionales
    xmlhttp2.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) { //si el llamado se realizo bien entonces:
            var myArr = JSON.parse(this.responseText); // se paresea la respuesta en myArr
            cargarAlertasNacionales(myArr); //se llama a la funcion para cargar las alertas nacionales en la pagina
        }
    };
    // esto se ejecuta cuando se reliza el llamado xmlhttp del clima
    xmlhttp3.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) { //si el llamado se realizo bien entonces.
            var myArr = JSON.parse(this.responseText); //se parsea la respuesta en myArr
            cargarClima(myArr); //se llama a la funcion para cargar el clima en la pagina
        }
    };

    xmlhttp1.open("GET", url, true); //se reliza el llamado para las alertas locales
    xmlhttp1.send();

    xmlhttp2.open("GET", urlNac, true); //se realiza el llamado para las alertas nacionales
    xmlhttp2.send();

    xmlhttp3.open("GET", urlClima, true); //se realiza el llamado para el clima
    xmlhttp3.send();

    //fucnion que se ejecuta con el resultado de el llamado de las alertas locales
    function cargarAlertaslocales(arr) {
        var alertas = '';
        var i;
        //el siguiente for se realiza una vez por cada alerta dentro de la respuesta
        for (i = 0; i < arr.alerts.length; i++) {
            var zonas = '';
            var j;
            // este for se ejecuta para guardar las zonas de la alerta actual y les da forma
            for (j = 0; j < Object.keys(arr.alerts[i].zones).length; j++) {
                zonas += arr.alerts[i].zones[j];
                if (j < Object.keys(arr.alerts[i].zones).length - 2) {
                    zonas += ', '
                } else {
                    if (j < Object.keys(arr.alerts[i].zones).length - 1) {
                        zonas += ' y '
                    } else {
                        zonas += '.'
                    }
                }
            }

            //ahora se le da forma de html a cada alerta y se concatenan en la variable alertas
            //que contiene el todas las alertas con formato html para pasarle a alertas.html
            alertas += '<br><div class="text-white bg-secondary "><div class="card-body"> <h5 class="card-title">  Tipo de alerta: ' +
                arr.alerts[i].title +
                ' </h5> <div  class="card-text" > Zonas afectadas: ' +
                zonas +
                ' </div> <div class="card-text"> Descripcion: ' +
                arr.alerts[i].description +
                '</div> </div></div> ';
        }
        document.getElementById("alertasLocales").innerHTML = alertas; //se le pasa como codigo html la variable alertas
        //a el elemento del htnl con id "alertasLocales"
    }


    //fucnion que se ejecuta con el resultado de el llamado de las alertas nacionales
    function cargarAlertasNacionales(arr) {
        var alertas = '';
        var i;
        //el siguiente for se realiza una vez por cada alerta dentro de la respuesta
        for (i = 0; i < arr.length; i++) {
            var zonas = '';
            var j;
            // este for se ejecuta para guardar las zonas de la alerta actual y les da forma
            for (j = 0; j < Object.keys(arr[i].zones).length; j++) {
                zonas += arr[i].zones[j];
                if (j < Object.keys(arr[i].zones).length - 2) {
                    zonas += ', '
                } else {
                    if (j < Object.keys(arr[i].zones).length - 1) {
                        zonas += ' y '
                    } else {
                        zonas += '.'
                    }
                }
            }
            //ahora se le da forma de html a cada alerta y se concatenan en la variable alertas
            //que contiene el todas las alertas con formato html para pasarle a alertas.html
            alertas += '<br><div class="text-white bg-secondary "><div class="card-body"> <h5 class="card-title">  Tipo de alerta: ' +
                arr[i].title +
                ' </h5> <div  class="card-text" > Zonas afectadas: ' +
                zonas +
                ' </div> <div class="card-text"> Descripcion: ' +
                arr[i].description +
                ' </div> </div></div> ';
        }
        document.getElementById("alertasNacionales").innerHTML = alertas; //se le pasa como codigo html la variable alertas
        //a el elemento del htnl con id "alertasLocales"
    }


    // el siguiente codigo se ejecuta con el llamado del estado del clima
    function cargarClima(arr) {
        var respuesta;
        var climaActual;
        var i;
        //la repsuesta dada contiene el el clima de  muchas zonas del pais, en este codigo se busca solamente
        //el estado del clima de san miguel
        for (i = 0; i < arr.length; i++) {
            if (arr[i].name === "San Miguel") {
                climaActual = i; // se guarda en clima actual la posicion en el arreglo que contiene el clima de san miguel
            }
        }


        // se le da formato html al resultado
        respuesta = '<br><div class="text-white bg-primary "><div class="card-body"> <h5 class="card-title">  Clima Actual en San Miguel ' +
            ' </h5> <div  class="card-text" ><h1> ' +
            arr[climaActual].weather.temp +
            '° </h1></div> <div class="card-text">  ' +
            arr[climaActual].weather.description +
            ' </div> <div class="card-text"> Humedad: ' +
            arr[climaActual].weather.humidity +
            ' % </div> </div></div> '


        //se pasa el codigo html a el elemento en alertas.html con id "clima"
        document.getElementById("clima").innerHTML = respuesta;

    }


}
$(inicio);