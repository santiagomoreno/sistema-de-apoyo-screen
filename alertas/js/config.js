var heroku = "https://assistanceservices.herokuapp.com/api";
var nac = "https://ws.smn.gob.ar/alerts/type/AL";
var local = "http://localhost:3000/api/";
var clima = "https://ws.smn.gob.ar/map_items/weather";

var Config = {
    urllocal: heroku,
    urlnac: nac,
    urlclima: clima
}